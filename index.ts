import { config } from 'dotenv';

enum Status {
  SUCCESS = 'SUCCESS'
}

type Response = {
  status: Status;
  value: string;
};

type Request = {
  password?: string;
}

export async function runMePlease(params: Request): Promise<Response | never> {
  if (!process.env.NODE_ENV) {
    throw new Error('Environment name was not provided.');
  }

  try {
    config({
      path: `./node_modules/test-utils/.env.${process.env.NODE_ENV}`
    });
  } catch {
    throw new Error('Environment name is invalid');
  }

  if (!params || params.password !== process.env.PASSWORD) {
    throw new Error('Incorrect password for used environment.');
  }

  return {
    status: Status.SUCCESS,
    value: 'Your application was able to run this function. You are victory!'
  };

}
